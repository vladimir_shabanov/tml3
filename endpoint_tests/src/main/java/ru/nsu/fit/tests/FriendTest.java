package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.SearchScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.nsu.fit.tests.Screen.UserScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Vladimir on 19.12.2016.
 */
public class FriendTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
    @Test
    @Title("Add friend")
    @Description("Add friend via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Adding friend feature")
    public void addFriend() throws InterruptedException {

        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin("vavanrrrage@gmail.com", "hello12345");
        Thread.sleep(5000);

        SearchScreen searchScreen = startScreen.search("Rontok");
        Thread.sleep(5000);

        //browser.click(By.xpath("//*[@id=\"branding-layout\"]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/a"));
        UserScreen userScreen = searchScreen.clickOnFoundedHeader();
        Thread.sleep(5000);
        userScreen.clickOnAddFriend();
        //browser.click(By.xpath("//*[@id=\"branding-layout\"]/div[1]/div[2]/div[1]/div/div[1]/div[2]/div[1]/table/tbody/tr[3]/td/div/span/i"));
        Thread.sleep(5000);
        Assert.assertEquals(userScreen.getResultAdding().substring(0, 1), "В");
        //Assert.assertEquals(browser.getElement(By.xpath("//*[@id=\"branding-layout\"]/div[1]/div[2]/div[1]/div/div[1]/div[2]/div[1]/table/tbody/tr[3]/td/p")).getText().substring(0, 1), "В");
        //System.out.println(browser.getElement(By.xpath("//*[@id=\"branding-layout\"]/div[1]/div[2]/div[1]/div/div[1]/div[2]/div[1]/table/tbody/tr[3]/td/p")).getText().substring(0, 1));
    }
}
