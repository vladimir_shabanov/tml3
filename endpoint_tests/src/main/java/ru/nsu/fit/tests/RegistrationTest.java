package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.RegistrationScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Vladimir on 19.12.2016.
 */
public class RegistrationTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
    public RegistrationScreen tryRegister(String eMail, String login, String password, String confirmPassword) {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();

        RegistrationScreen registrationScreen = startScreen.clickOnRegistration();
        registrationScreen.tryRegister(eMail, login, password, confirmPassword);

        return registrationScreen;
    }
    @Test
    @Title("FailForRegistrationByEmail")
    @Description("Test registration with fail result because of wrong e-mail via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail registration by Email feature")
    public void failForRegistrationByEmail() throws InterruptedException {
        RegistrationScreen registrationScreen = tryRegister("vavanrrrage@gmail.com", "vavanrrrage1", "hello1234", "hello1234");
        Assert.assertEquals(registrationScreen.getErrorRegistrationMessage(), "Пользователь с таким адресом уже существует");
    }

    @Test
    @Title("FailForRegistrationByPassword")
    @Description("Test registration with fail result because of password via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail registration by Password feature")
    public void failForRegistrationByPassword() throws InterruptedException {
        RegistrationScreen registrationScreen = tryRegister("vavanrrragert@gmail.com", "vavanrrrage1", "hello1234", "hello123");
        Assert.assertEquals(registrationScreen.getErrorRegistrationMessage(), "Не совпадают поля паролей");
    }

    @Test
    @Title("FailForRegistrationByLogin")
    @Description("Test registration with fail result because of existing login via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail registration by Login feature")
    public void failForRegistrationByLogin() throws InterruptedException {
        RegistrationScreen registrationScreen = tryRegister("vavanrrragert@gmail.com", "vavanrrrage", "hello1234", "hello1234");
        Assert.assertEquals(registrationScreen.getErrorRegistrationMessage(), "Пользователь с таким именем на сайте уже существует");
    }
}
