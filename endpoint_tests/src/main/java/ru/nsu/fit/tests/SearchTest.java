package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.Screen.SearchScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Vladimir on 19.12.2016.
 */
public class SearchTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }

    @Test
    @Title("SuccessSearch")
    @Description("Test search with success result via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Success search feature")
    public void successSearch() throws InterruptedException {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        SearchScreen searchScreen = startScreen.search("Зенит");
        Assert.assertEquals(searchScreen.getResultSearchTitle(), "Зенит");
    }

    @Test
    @Title("FailSearch")
    @Description("Test search with fail result via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail search feature")
    public void failSearch() throws InterruptedException {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        SearchScreen searchScreen = startScreen.search("njnbhf");

        Assert.assertEquals(searchScreen.getNotFoundMessage(), "Ничего не найдено");
    }

}
