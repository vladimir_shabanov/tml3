package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

/**
 * Created by Александр on 24.12.2016.
 */
public class FeedScreen {
    private Browser browser;
    public FeedScreen(Browser browser) {
        this.browser = browser;
    }
    public String getSubscribeMessage() {
        String subscribeMessageXPath = "//*[@id=\"branding-layout\"]/div[1]/div/div/main/div/header/p";
        return browser.getElement(By.xpath(subscribeMessageXPath)).getText();
    }
}
