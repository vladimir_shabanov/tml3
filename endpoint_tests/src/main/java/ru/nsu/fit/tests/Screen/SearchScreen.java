package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

/**
 * Created by Александр on 24.12.2016.
 */
public class SearchScreen {
    private Browser browser;
    private final String SEARCH_TITLE_XPATH = "//*[@id=\"branding-layout\"]/div/div[2]/div/div/div[1]/div[2]/div[1]/div[2]/h1/a";
    public SearchScreen(Browser browser) {
        this.browser = browser;
    }
    public String getResultSearchTitle() {

        return browser.getElement(By.xpath(SEARCH_TITLE_XPATH)).getText();
    }
    public String getNotFoundMessage() {
        String messageNotFoundXPath = "//p[text() = 'Ничего не найдено']";
        return browser.getElement(By.xpath(messageNotFoundXPath)).getText();
    }
    public UserScreen clickOnFoundedHeader() {
        browser.click(By.xpath("//a[@href='https://www.sports.ru/profile/1029634516/']"));
        return new UserScreen(browser);
    }
    /*public void waitAppearTitle() {
        browser.waitForElement(By.xpath(SEARCH_TITLE_XPAH));
    }*/
}
