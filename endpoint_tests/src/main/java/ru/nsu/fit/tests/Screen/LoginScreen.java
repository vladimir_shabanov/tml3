package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

/**
 * Created by Александр on 23.12.2016.
 */
public class LoginScreen {
    private Browser browser;
    public LoginScreen(Browser browser) {
        this.browser = browser;
    }
    private void fillLoginPassword(String login, String password) {
        String loginXPath = "//div[1]/div/form/div[2]/label/input";
        String passwordXPath = "//div[1]/div/form/div[3]/label/input";
        browser.getElement(By.xpath(loginXPath)).sendKeys(login);
        browser.getElement(By.xpath(passwordXPath)).sendKeys(password);
        AllureUtils.saveImageAttach("sports.ru ввод данных", browser.makeScreenshot());
        AllureUtils.saveTextLog("Data entered");
    }
    public void tryLogin(String login, String password) {
        fillLoginPassword(login, password);

        String enterButtonXPath = "//button[text() = 'Войти']";

        browser.click(By.xpath(enterButtonXPath));

        AllureUtils.saveImageAttach("sports.ru Вход", browser.makeScreenshot());
        AllureUtils.saveTextLog("Войти pushed");
    }
    public String getCurrentUser() {
        String currentUserXPath = "/html/body/div[1]/div/div/div/ul/li[1]/a";
        return browser.getElement(By.xpath(currentUserXPath)).getText();
    }
    public void waitChangeUser() {
        String currentUserXPath = "/html/body/div[1]/div/div/div/ul/li[1]/a";
        browser.waitForElement(By.xpath(currentUserXPath));
    }
    public String getErrorLoginMessage() {
        String errorClassName = "auth__message-error";
        return browser.getElement(By.className(errorClassName)).getText();
    }
}
