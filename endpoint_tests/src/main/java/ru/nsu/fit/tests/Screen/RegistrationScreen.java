package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

/**
 * Created by Александр on 23.12.2016.
 */
public class RegistrationScreen {
    private Browser browser;

    private final String EMAIL_XPATH = "//input[@placeholder = 'Ваш e-mail' and @class='auth__reg-input input input_size_extra-large']";
    private final String PASSWORD_XPATH = "//input[@placeholder = 'Пароль' and @class='auth__reg-input input input_size_extra-large']";
    private final String CONFIRM_PASSWORD_XPATH = "//input[@placeholder = 'Пароль еще раз' and @class='auth__reg-input input input_size_extra-large']";
    private final String LOGIN_XPATH = "//input[@placeholder = 'Имя на сайте' and @class='auth__reg-input input input_size_extra-large']";
    private final String REGISTER_BUTTON_XPATH = "//button[text() = 'Зарегистрироваться']";
    private final String ERROR_REGISTRATION_MESSAGE_XPATH = "//div[2]/div/form/div[5]/div/span[1]";
    public RegistrationScreen(Browser browser) {
        this.browser = browser;
    }
    private void fillFields(String eMail, String login, String password, String confrimPassword) {

        browser.getElement(By.xpath(EMAIL_XPATH)).sendKeys(eMail);
        browser.getElement(By.xpath(PASSWORD_XPATH)).sendKeys(password);
        browser.getElement(By.xpath(LOGIN_XPATH)).sendKeys(login);
        browser.getElement(By.xpath(CONFIRM_PASSWORD_XPATH)).sendKeys(confrimPassword);
        AllureUtils.saveImageAttach("sports.ru ввод данных", browser.makeScreenshot());
        AllureUtils.saveTextLog("Data entered");
    }
    public void tryRegister(String eMail, String login, String password, String confirmPassword) {
        fillFields(eMail, login, password, confirmPassword);
        browser.click(By.xpath(REGISTER_BUTTON_XPATH));

        AllureUtils.saveImageAttach("sports.ru Регистрация", browser.makeScreenshot());
        AllureUtils.saveTextLog("Зарегистрироваться pushed");
    }
    public String getErrorRegistrationMessage() {
        return browser.getElement(By.xpath(ERROR_REGISTRATION_MESSAGE_XPATH)).getText();
    }
}
