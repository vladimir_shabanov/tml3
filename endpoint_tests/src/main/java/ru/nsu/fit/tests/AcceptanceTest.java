package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */

public class AcceptanceTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("SuccessForLogIn")
    @Description("Test login with success result via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Success login feature")
    public void successForLogIn() throws InterruptedException {

        LoginScreen loginScreen = tryLoginWithStartPageRunning("vavanrrrage@gmail.com", "hello12345");

        //*[contains(text(),'dsdfsdf')]
        loginScreen.waitChangeUser();
        Assert.assertEquals(loginScreen.getCurrentUser(), "vavanrrrage");
    }

    @Test
    @Title("FailForLogInByPassword")
    @Description("Test login with fail result because of wrong password via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail login by password feature")
    public void failForLogInByPassword() throws InterruptedException {

        LoginScreen loginScreen = tryLoginWithStartPageRunning("vavanrrrage@gmail.com", "12345");

        Assert.assertEquals(loginScreen.getErrorLoginMessage(), "Некорректный логин/пароль, попробуйте еще раз");
    }


    @Test
    @Title("FailForLogInByLoginName")
    @Description("Test login with fail result because of wrong login via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Fail login by LoginName")
    public void failForLogInByLogin() throws InterruptedException {

        LoginScreen loginScreen = tryLoginWithStartPageRunning("vavanrrragegmail.com", "hello12345");

        Assert.assertEquals(browser.getElement(By.className("auth__message-error")).getText(), "Некорректный логин/пароль, попробуйте еще раз");
    }

    public LoginScreen tryLoginWithStartPageRunning(String login, String password) {
        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();

        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin(login, password);

        return loginScreen;
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
