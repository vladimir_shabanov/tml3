package ru.nsu.fit.tests.Screen;

import com.sun.jna.platform.win32.Netapi32Util;
import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

/**
 * Created by Александр on 24.12.2016.
 */
public class UserScreen {
    private Browser browser;
    public UserScreen(Browser browser) {
        this.browser = browser;
    }
    public void clickOnAddFriend() {
        browser.click(By.xpath("//i[text() = 'Добавить в друзья']"));
    }
    public String getResultAdding() {
        return browser.getElement(By.xpath("//td/p")).getText();
    }
}
