package ru.nsu.fit.tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.tests.Screen.FeedScreen;
import ru.nsu.fit.tests.Screen.LoginScreen;
import ru.nsu.fit.tests.Screen.StartScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Vladimir on 19.12.2016.
 */
public class FeedTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }
    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
    @Test
    @Title("SuccessForFeed")
    @Description("Get Feed via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Success getting Feed feature")
    public void successForFeed() throws InterruptedException {

        StartScreen startScreen = new StartScreen(browser);
        startScreen.openStartPage();
        System.out.println(browser.getElement(By.xpath("/html/body/div[1]/div/div/div/div/span/a")).getText());
        LoginScreen loginScreen = startScreen.clickOnLogin();
        loginScreen.tryLogin("vavanrrrage@gmail.com", "hello12345");
        System.out.println(browser.getElement(By.xpath("/html/body/div[1]/div/div/div/div/span/a")).getText());
//
        //browser.waitForElement(By.xpath("//span[contains(text(),'Моя лента')]"));
        //startScreen.waitChangeFeedButton("Моя лента");
        Thread.sleep(5000);
        //System.out.println("Lol " + browser.getElement(By.xpath("//span[contains(.,'Моя лента')")).getText());
        //browser.click(By.xpath("/html/body/div[1]/div/div/div/div/span/a"));
        FeedScreen feedScreen = startScreen.clickOnMyNews();

        Thread.sleep(3000);

        Assert.assertEquals(feedScreen.getSubscribeMessage(), "Подписывайтесь на пользователей, блоги и теги, чтобы собрать разнообразную и интересную ленту.");
        //Assert.assertEquals(browser.getElement(By.xpath("//*[@id=\"branding-layout\"]/div[1]/div/div/main/div/header/p")).getText(), "Подписывайтесь на пользователей, блоги и теги, чтобы собрать разнообразную и интересную ленту.");
    }
}
