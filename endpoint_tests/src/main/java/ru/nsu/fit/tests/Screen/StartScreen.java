package ru.nsu.fit.tests.Screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;

/**
 * Created by Vladimir on 19.12.2016.
 */
public class StartScreen {
    private Browser browser;
    public StartScreen(Browser browser) {
        this.browser = browser;
    }

    public void openStartPage() {
        String startPageUrl = "https://www.sports.ru/";

        browser.openPage(startPageUrl);
        AllureUtils.saveImageAttach("sports.ru", browser.makeScreenshot());
        AllureUtils.saveTextLog("Page loaded");
    }
    public LoginScreen clickOnLogin() {
        String loginXPath = "//a[@href=\"/logon.html\"]";
        By byLogin = By.xpath(loginXPath);
        browser.click(byLogin);
        AllureUtils.saveImageAttach("sports.ru Войти", browser.makeScreenshot());
        AllureUtils.saveTextLog("Войти pushed");

        return new LoginScreen(browser);
    }
    public RegistrationScreen clickOnRegistration() {
        String registrationXPath = "//a[@href='#']";
        By byRegistration = By.xpath(registrationXPath);

        browser.click(byRegistration);
        AllureUtils.saveImageAttach("sports.ru Регистрация", browser.makeScreenshot());
        AllureUtils.saveTextLog("Регистрация pushed");

        return new RegistrationScreen(browser);
    }
    public SearchScreen search(String text) {
        String searchCommandXPath = "//header/div/div[1]/div/form/input";
        String buttonSearchXPath = "//header/div/div[1]/div/form/button";
        browser.typeText(By.xpath(searchCommandXPath), text);
        browser.click(By.xpath(buttonSearchXPath));
        return new SearchScreen(browser);
    }
    public FeedScreen clickOnMyNews() {
        String myNewsXPath = "/html/body/div[1]/div/div/div/div/span/a";
        browser.click(By.xpath(myNewsXPath));
        return new FeedScreen(browser);
    }
    public void waitChangeFeedButton(String text) {
        String whatWait = "/html/body/div[1]/div/div/div/div/span/a" + "[contains(" + text + ")]";
        browser.waitForElement(By.xpath(whatWait));
    }
}
